package com.study.dmitrij.gyrogame

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.FrameLayout
import android.widget.RadioButton
import android.widget.RelativeLayout
import java.lang.Math.*

class PlayerActivity : AppCompatActivity(), SensorEventListener {
    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null)
            return
        inputXVal = event.values[0]
        inputYVal = event.values[1]

    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    //val ball:android.widget.TextView=findViewById(R.id.radio) as android.widget.TextView
    //val timer= kotlin.concurrent.timer("tickTimer",false,0,(1000/60),{ do_update() })
    var ball: RadioButton? = null
    //var field:Array<BooleanArray> =
    var inputXVal = 0f
    var inputYVal = 0f
    var counter = 0
    var ballPosX = 0f
    var ballPosY = 0f

    var ballVelocityX = 0f
    var ballVelocityY = 0f

    fun getInputX(): kotlin.Float {
        return -inputXVal
    }

    fun getInputY(): kotlin.Float {
        return inputYVal
    }

    val velocityDampening: Float = .8f
    val reflectVelocity: Float = .7f
    val fieldWidth: Int = 20
    var fieldCellSize: Int = 32
    val fieldHeight: Int = 24
    val inputScale: Float = .01f

    fun update() {
        val x = getInputX()
        val y = getInputY()
        if (abs(x) > .01f)
            ballVelocityX += x * inputScale
        else
            ballVelocityX *= velocityDampening
        if (abs(y) > .01f)
            ballVelocityY += y * inputScale
        else
            ballVelocityY *= velocityDampening

        val newPosX = ballPosX + ballVelocityX
        val newPosY = ballPosY + ballVelocityY
        if (abs(fieldWidth / 2 - newPosX) > fieldWidth / 2) {
            ballVelocityX = -ballVelocityX * reflectVelocity
            ballPosX += signum(ballVelocityX) * (abs(newPosX - fieldWidth / 2) - fieldWidth / 2)
        } else
            ballPosX = newPosX


        if (abs(fieldHeight / 2 - newPosY) > fieldHeight / 2) {
            ballVelocityY = -ballVelocityY * reflectVelocity
            ballPosY += signum(ballVelocityY) * (abs(newPosY - fieldHeight / 2) - fieldHeight / 2)
        } else
            ballPosY = newPosY

        if (ball != null && ball is RadioButton) {
            ball!!.x = ballPosX * fieldCellSize
            ball!!.y = ballPosY * fieldCellSize
        }
    }

    fun do_update() {
        update()
        counter += 1
        val run = Runnable { kotlin.run { do_update() } }
        loopHandler.postDelayed(
                run, 1000 / 60
        )
    }

    val loopHandler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val manager = (getSystemService(Context.SENSOR_SERVICE) as SensorManager)
        val sensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)


        manager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME)

        setContentView(R.layout.activity_player)

        ball = findViewById(R.id.ball) as RadioButton

        val frame = this.findViewById(R.id.my_frame_layout_1)
        val lp = FrameLayout.LayoutParams(
                (fieldWidth + 1) * fieldCellSize,
                (fieldHeight + 1) * fieldCellSize
        )

        frame.layoutParams = lp

        val run = Runnable { kotlin.run { do_update() } }
        loopHandler.postDelayed(
                run, 1000 / 60
        )
    }
}
